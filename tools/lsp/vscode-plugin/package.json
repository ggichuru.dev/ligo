{
    "name": "ligo-vscode",
    "description": "LIGO VSCode extension includes syntax highlighting, and a language server for LIGO.",
    "author": "Serokell",
    "license": "MIT",
    "version":  "0.4.9",
    "repository": {
        "type": "git",
        "url": "https://gitlab.com/ligolang/ligo"
    },
    "publisher": "ligolang-publish",
    "categories": [
        "Programming Languages"
    ],
    "keywords": [],
    "engines": {
        "vscode": "^1.57.0"
    },
    "activationEvents": [
        "onLanguage:ligo",
        "onLanguage:mligo",
        "onLanguage:religo"
    ],
    "icon": "logo.png",
    "main": "./client/out/extension",
    "contributes": {
        "languages": [
            {
                "id": "ligo",
                "aliases": [
                    "PascaLIGO"
                ],
                "extensions": [
                    ".ligo"
                ],
                "icon": {
                    "light": "logo.png",
                    "dark": "logo.png"
                },
                "configuration": "./syntaxes/ligo.configuration.json"
            },
            {
                "id": "mligo",
                "aliases": [
                    "CameLIGO"
                ],
                "extensions": [
                    ".mligo"
                ],
                "icon": {
                    "light": "logo.png",
                    "dark": "logo.png"
                },
                "configuration": "./syntaxes/mligo.configuration.json"
            },
            {
                "id": "religo",
                "aliases": [
                    "ReasonLIGO"
                ],
                "extensions": [
                    ".religo"
                ],
                "icon": {
                    "light": "logo.png",
                    "dark": "logo.png"
                },
                "configuration": "./syntaxes/religo.configuration.json"
            }
        ],
        "grammars": [
            {
                "language": "ligo",
                "scopeName": "source.ligo",
                "path": "./syntaxes/ligo.tmLanguage.json"
            },
            {
                "language": "mligo",
                "scopeName": "source.mligo",
                "path": "./syntaxes/mligo.tmLanguage.json"
            },
            {
                "language": "religo",
                "scopeName": "source.religo",
                "path": "./syntaxes/religo.tmLanguage.json"
            }
        ],
        "configuration": {
            "type": "object",
            "title": "LIGO LSP Configuration",
            "properties": {
                "ligoLanguageServer.maxNumberOfProblems": {
                    "scope": "resource",
                    "type": "number",
                    "default": 100,
                    "description": "Controls the maximum number of problems produced by the server."
                },
                "ligoLanguageServer.ligoBinaryPath": {
                    "scope": "resource",
                    "type": "string",
                    "default": "ligo",
                    "description": "Path to the ligo binary, default one is extracted from $PATH variable."
                },
                "ligoLanguageServer.disabledFeatures": {
                    "scope": "resource",
                    "type": "array",
                    "default": [],
                    "description": "Disable specific LIGO LSP features. For a list of features, see [the README.md](https://gitlab.com/serokell/ligo/ligo/-/blob/tooling/tools/lsp/vscode-plugin/README.md)."
                }
            }
        },
        "commands": [
            {
                "command": "ligo.startServer",
                "title": "LIGO: Start LIGO LSP server",
                "description": "Starts the LIGO LSP server"
            },
            {
                "command": "ligo.stopServer",
                "title": "LIGO: Stop LIGO LSP server",
                "description": "Stops the LIGO LSP server"
            },
            {
                "command": "ligo.restartServer",
                "title": "LIGO: Restart LIGO LSP server",
                "description": "Stops then starts the LIGO LSP server"
            }
        ]
    },
    "scripts": {
        "vscode:prepublish": "npm run esbuild-base -- --minify",
        "compile": "npm run esbuild-base -- --sourcemap",
        "esbuild-base": "esbuild ./client/src/extension.ts --bundle --outfile=./client/out/extension.js --external:vscode --format=cjs --platform=node",
        "watch": "tsc -b -w",
        "package": "vsce package",
        "lint": "eslint client --ext .ts",
        "build-tests": "tsc -p ./test/",
        "test": "npm run build-tests && node client/out/test/driver.js"
    },
    "dependencies": {
        "axios": "^0.21.1",
        "semver": "^7.3.5",
        "vscode-languageclient": "^7.0.0"
    },
    "devDependencies": {
        "@types/mocha": "^9.0.0",
        "@types/node": "^15.12.2",
        "@types/vscode": "^1.57.0",
        "@typescript-eslint/eslint-plugin": "^4.27.0",
        "@typescript-eslint/parser": "^4.27.0",
        "@vscode/test-electron": "^1.6.1",
        "esbuild": "^0.13.4",
        "eslint": "^7.28.0",
        "eslint-config-airbnb-base": "^14.2.1",
        "eslint-plugin-import": "^2.23.4",
        "glob": "^7.2.0",
        "mocha": "^9.1.3",
        "ts-command-line-args": "^2.1.0",
        "typescript": "^4.3.2",
        "vsce": "^1.93.0"
    }
}
